




//Add items to the cart
jQuery(document).ready(function(){
	jQuery(document).on("ajax_image", function () {





		jQuery.ajax({
			url: MyAjax.ajaxurl,
			data: {
				'action':'cart_ajax_request',
				'ajax_category_id' : ajax_category_id,
			},
			success:function(data) {

				// This outputs the result of the ajax request

				//add the ajaxed cart contents into the html containers
				jQuery( ".cart_interior" ).empty();
				jQuery( ".cart_interior" ).append(data).hide().fadeIn(250);


				//Add in the text version of the ajax content for the email
				var text_content = jQuery('.text_products').html();
				jQuery( "#wpcf7-f530-o1 #text_products_list" ).html('');
				jQuery( "#wpcf7-f530-o1 #text_products_list" ).append(text_content);

				//count the products for the nav bar
				var cart_count = jQuery( ".cart .product" ).length;
				jQuery( ".cart_count" ).empty();
				jQuery( ".cart_count" ).append(cart_count);
			},
			error: function(errorThrown){
				console.log(errorThrown);
			}
		});
	});
});

// We'll pass this variable to the PHP function cart_ajax_request


// This does the ajax request
