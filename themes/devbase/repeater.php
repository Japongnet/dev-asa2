
    <div class="section bg_cover <?php echo $rows[($counter % $row_count)]['page_section_class']; ?>" style="background-image:url('<?php echo $rows[($counter % $row_count)]['page_section_background'];?>')">
			<div class="section_content">
				<div class="section_content <?php echo $rows[($counter % $row_count)]['page_section_orientation']; ?>">
					<div class="section_content_main ">
						<div class="section_content_text">
							<?php if(  $rows[($counter % $row_count)]['page_section_title'] ): ?>
								<h2 class="section_title"><?php echo $rows[($counter % $row_count)]['page_section_title']; ?></h2>
							<?php endif; ?>
							<?php if(  $rows[($counter % $row_count)]['page_section_tagline'] ): ?>
								<em class="section_tagline"><?php echo $rows[($counter % $row_count)]['page_section_tagline']; ?></em>
							<?php endif; ?>
							<?php if(  $rows[($counter % $row_count)]['page_section_text'] ): ?>
								<div class="section_text"><?php echo $rows[($counter % $row_count)]['page_section_text']; ?></div>
							<?php endif; ?>
							<?php if(  $rows[($counter % $row_count)]['page_section_button'] ): ?>
								<button class="section_button"><?php echo $rows[($counter % $row_count)]['page_section_button']; ?></button>
							<?php endif; ?>
						</div>
						<?php if(  $rows[($counter % $row_count)]['page_section_ribbon'] ): ?>
						<div class="section_content_ribbons">
							<img class="section_image" src="<?php echo $rows[($counter % $row_count)]['page_section_ribbon']; ?>" alt="section_ribbon">
						</div>
						<?php endif; ?>
					</div>
					<?php if(  $rows[($counter % $row_count)]['page_section_image'] ): ?>
					<div class="section_content_image">
							<img class="section_image" src="<?php echo $rows[($counter % $row_count)]['page_section_image']; ?>" alt="section_image">
					</div>
					<?php endif; ?>
				</div>

			</div>
		</div>
