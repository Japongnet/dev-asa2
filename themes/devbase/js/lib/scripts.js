(function ($, root, undefined) {

	$(function () {

		'use strict';

		// DOM ready, take it away

	});

})(jQuery, this);



jQuery( document ).ready(function() {
	var waypoints = jQuery('.header_toggle').waypoint({
		handler: function(direction) {
			jQuery( ".menu_section" ).toggleClass( "sticky" )
			jQuery( ".menu_section" ).removeClass( "sticky_bottom" )
		}
	});
});




jQuery( document ).ready(function() {

	var waypoints = jQuery('.waypoint').waypoint({

		handler: function(direction) {;

			jQuery(this.element).addClass('waypoint_loaded').trigger("swap_image");
			this.destroy();
		}

	});
});






jQuery( document ).ready(function() {

	var waypoints = jQuery('.waypoint').waypoint({
		offset:"50%",
		handler: function(direction) {

			jQuery('.waypoint_active').removeClass('waypoint_active');
			jQuery(this.element).addClass('waypoint_active');


		}

	});
	var waypoints = jQuery('.waypoint').waypoint({
		offset:"-25%",
		handler: function(direction) {
			if(direction === "up") {
				jQuery('.waypoint_active').removeClass('waypoint_active');
				jQuery(this.element).addClass('waypoint_active');
			}

		}

	});


});


var ajax_category_id;

jQuery( document ).ready(function() {

	var waypoints = jQuery('.category_waypoint').waypoint({

		handler: function(direction) {

			if(direction === "down") {
				jQuery('.category_waypoint_active').removeClass('category_waypoint_active');


				jQuery(this.element).addClass('category_waypoint_active');
				jQuery('.video_active').removeClass('video_active');
				jQuery('.category_waypont_active .page_category_video').addClass('video_active');
				jQuery('.video_active').trigger('pause');
				jQuery('.video_active').attr('currentTime', 0);
				jQuery('.video_active').trigger('play');
				ajax_category_id = jQuery('.category_waypoint_active').attr('id');
				jQuery('.link_active').removeClass('link_active');
				jQuery('.test_scroll.' + jQuery('.category_waypoint_active').attr('id')).addClass('link_active');
				jQuery('.category_waypoint_active').trigger('ajax_image');
			}
		}

	});

	var waypoints = jQuery('.category_waypoint_bottom').waypoint({
		handler: function(direction) {
			if(direction === "up") {
				jQuery('.category_waypoint_active').removeClass('category_waypoint_active');
				jQuery('.video_active').removeClass('video_active');
				jQuery(this.element).parent().addClass('category_waypoint_active');
				ajax_category_id = jQuery('.category_waypoint_active').attr('id');
				jQuery('.page_category_video').addClass('video_active');
				jQuery('.video_active').trigger('pause');
				jQuery('.video_active').attr('currentTime', 0);
				jQuery('.video_active').trigger('play');
				jQuery(this.element).addClass('category_waypoint_active');
				jQuery('.link_active').removeClass('link_active');
				jQuery('.test_scroll.' + jQuery('.category_waypoint_active').attr('id')).addClass('link_active');
				jQuery('.category_waypoint_active').trigger('ajax_image');

			}
		}

	});
});

jQuery( document ).ready(function() {
	jQuery('.toggle_description').click(function(){
		event.preventDefault();
		jQuery('.section_long_description').toggleClass('active');
	});
});

jQuery(document).on("swap_image", function () {
	jQuery('.waypoint_loaded img.placeholder_image').each(function () {
		var $this = jQuery(this),
		newSrc = $this.attr('data-alternate-src');
		$this.fadeOut(  function() {
			$this.attr('src', newSrc);
		});
		$this.fadeIn();
		jQuery( ".waypoint_loaded img.placeholder_image" ).removeClass( "placeholder_image" );
	});
	jQuery('.waypoint_loaded.placeholder_bg').each(function () {
		var $this = jQuery(this),
		newSrc = $this.attr('data-alternate-src');
		$this.fadeOut(  function() {
			$this.attr('style', newSrc);
		});
		$this.fadeIn();
		jQuery( ".waypoint_loaded.placeholder_bg").removeClass( "placeholder_bg" );
	});
});

jQuery('.swap_button').click(function swap( event ){
	event.preventDefault();
	jQuery('img').each(function () {
		var $this = jQuery(this),
		newSrc = $this.attr('data-alternate-src');
		$this.fadeOut(  function() {
			$this.attr('data-alternate-src', $this.attr('src'));
			$this.attr('src', newSrc);
		});
		$this.fadeIn();
		objectFitImages();
	});
});




jQuery('a[href*="#"]')
// Remove links that don't actually link to anything
.not('[href="#"]')
.not('[href="#0"]')
.click(function(event) {
	// On-page links
	if (
		location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
		&&
		location.hostname == this.hostname
	) {
		// Figure out element to scroll to
		var target = jQuery(this.hash);
		target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
		// Does a scroll target exist?
		if (target.length) {
			// Only prevent default if animation is actually gonna happen
			event.preventDefault();
			console.log('no ajax scroll!');
			jQuery('html, body').animate({
				scrollTop: target.offset().top
			}, 500, function() {
				// Callback after animation
				// Must change focus!
				var jQuerytarget = jQuery(target);
				jQuerytarget.focus();
				if (jQuerytarget.is(":focus")) { // Checking if the target was focused
					return false;
				} else {
					jQuerytarget.attr('tabindex','-1'); // Adding tabindex for elements not focusable
					jQuerytarget.focus(); // Set focus again
				};
			});
		}
	}
});



jQuery( document ).ready(function() {
	jQuery(function() {
		jQuery.scrollify({
			section : ".panel_test",
		});
		jQuery(document).on('click', '.next', function(event){
			event.preventDefault();
			jQuery.scrollify.next();
		});
	});
});

jQuery( document ).ready(function() {
	objectFitImages();
});
