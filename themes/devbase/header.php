<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>


        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/fonts/stylesheet.css">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>





	</head>
	<body <?php body_class(); ?>>




			<!-- header -->
			<header class="header clear" role="banner">
					<?php if ( is_front_page() ) {	?>
							<div class="header_home bg_cover" style="background-image:url('	<?php the_field('header_home', 27);?>')">
								<div class="section_content">
									<div class="header_home_logo bg_contain" style="background-image:url('<?php the_field('header_home_logo', 27);?>')"></div>

									<a href="<?php echo get_site_url(); ?>/contact/" class="background_highlight button">
										FIND OUT MORE
									</a>
								</div>
							</div>
							<div class="menu_section">
								<div class="section_content">
									<!-- nav -->
									<nav class="nav" role="navigation">
										<div class="header_home_logo_icon bg_contain" style="background-image:url('<?php the_field('header_home_logo', 27);?>')"></div>
										<?php 		wp_nav_menu(array ('menu' => 'main_menu'));
										?>
									</nav>
									<!-- /nav -->
								</div>
							</div>
						<?php } else { ?>
							<div class="header_home bg_cover" style="background-image:url('	<?php the_field('header_home', 27);?>')">
								<div class="section_content">
									<div class="header_home_logo bg_contain" style="background-image:url('<?php the_field('header_home_logo', 27);?>')"></div>

									<a href="<?php echo get_site_url(); ?>/contact/" class="background_highlight button">
										FIND OUT MORE
									</a>
								</div>
							</div>

						<?php }

					?>



			</header>
			<!-- /header -->
