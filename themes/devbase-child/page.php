<?php get_header(); ?>

<?php

include 'header_menu.php';




?>
<?php
$i = 0;
// check if the repeater field has rows of data
if( have_rows('page_sections_repeater') ): ?>

<?php // loop through the rows of data
while ( have_rows('page_sections_repeater') ) : the_row(); ?>







<div id="<?php the_sub_field('page_category_id') ?>"  class="category category_waypoint category_bg placeholder_bg <?php the_sub_field('page_category_class'); ?>"
  >
  <div class="panel_test page_category_interstitial <?php the_sub_field('page_category_interstitial_class'); ?>">
    <div class= "section waypoint">
      <div class="section_content">
        <div id="<?php the_sub_field('page_category_id') ?>_title" class="page_section_title"><h2><?php the_sub_field('page_category_title'); ?><br/>  <a href="#" class="home_next"><span class="next fa fa-arrow-circle-o-down"></span></a></h2></div>

        <?php the_sub_field('page_category_content'); ?>
        <?php the_sub_field('page_category_interstitial'); ?>

      </div>
    </div>
  </div>
  <?php if( get_sub_field('page_category_image') ): ?>

    <video class="page_category_video" id="<?php the_sub_field('page_category_id') ?>_video" src="<?php the_sub_field('page_category_image'); ?>" autoplay muted="muted">
      <script>
      document.getElementById('<?php the_sub_field('page_category_id') ?>_video').addEventListener('ended',myHandler,false);
      function myHandler(e) {
          console.log('ended!');
          jQuery(this).addClass('fade_grey');
          jQuery('#<?php the_sub_field('page_category_id') ?>_title').addClass('fade_in_transparency');

      }
      </script>

    </video>
  <?php endif; ?>
  <?php if( have_rows(
    'page_category_repeater') ): ?>


    <?php // loop through the rows of data
    while ( have_rows('page_category_repeater') ) : the_row(); ?>






    <?php

    include 'content_repeater.php';
    ?>

  <?php endwhile;

  else :

    // no rows found

  endif;

  ?>
  <span class="category_waypoint_bottom"></span>
</div> <!-- /category -->

<!--

-->


<?php endwhile; ?>


<?php

else :

  // no rows found

endif;

?>



<?php get_footer(); ?>
