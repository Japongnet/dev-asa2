

    <?php $page_category_repeater_image =  get_sub_field('page_category_repeater_image'); ?>

  <div id="<?php echo $page_category_repeater_image["title"] ?>"
  class="panel_test waypoint <?php the_sub_field('page_category_repeater_class'); ?>">
  <div class="section_overlay">
  </div>
  <div class="section_content">

    <div class="section_content_main ">

      <div class="section_content_text">
        <?php if( get_sub_field('page_category_repeater_title') ): ?>
          <div class="section_title"><h2><?php the_sub_field('page_category_repeater_title'); ?></h2></div>
        <?php endif; ?>
        <?php if( get_sub_field('page_section_tagline') ): ?>
          <em class="section_tagline"><?php the_sub_field('page_section_tagline'); ?></em>
        <?php endif; ?>
        <?php if( get_sub_field('page_category_repeater_text') ): ?>
          <div class="section_text">
            <?php the_sub_field('page_category_repeater_text'); ?></div>
          <?php endif; ?>
          <?php if( get_sub_field('page_category_repeater_long_description') ): ?>
            <div class="description_container">
              <a class="toggle_description" href="#">Read More +</a>
              <div class="section_long_description"><?php the_sub_field('page_category_repeater_long_description'); ?></div>
            </div>
          <?php endif; ?>
          <?php if( get_sub_field('page_section_button') ): ?>
            <button class="section_button"><?php the_sub_field('page_section_button'); ?></button>
          <?php endif; ?>
          <?php if( get_sub_field('page_category_repeater_image') ): ?>




            <img class="placeholder_image page_category_repeater_image" data-alternate-src="<?php echo $page_category_repeater_image["url"] ?>" src="<?php the_field('placeholder_image', 27); ?>" alt="<?php the_sub_field('page_category_repeater_text'); ?>">

            <?php endif; ?>
        </div>

      </div>



    </div>
  </div>
