<?php
/**
* Template Name: Blog Index
*
*/


get_header();

include 'header_menu.php';

?>

<?php
// the query
$the_query = new WP_Query( array( 'post_type' => 'post' ) ); ?>

<?php if ( $the_query->have_posts() ) : ?>



  <!-- pagination here -->

  <!-- the loop -->
  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    <?php the_title(); ?>

    <div class="section bg_cover <?php the_field('blog_section_class'); ?>" style="background-image:url('<?php the_field('blog_section_background');?>')">
      <div class="section_content">
        <div class="section_content <?php the_field('blog_section_orientation'); ?>">
          <div class="section_content_main ">
            <div class="section_content_text">

              <?php if( get_field('blog_section_title') ): ?>

                <h2  class="section_title">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                </h2><br/>
              <?php endif; ?>
              <?php if( get_field('blog_section_tagline') ): ?>
                <em class="section_tagline"><?php the_field('blog_section_tagline'); ?></em>
              <?php endif; ?>
              	<br/><span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
                <hr/>
              <?php if( get_field('blog_section_text') ): ?>
                <div class="section_text"><?php the_field('blog_section_excerpt'); ?></div>
              <?php endif; ?>
              <?php if( get_field('blog_section_button') ): ?>
                <button class="section_button"><?php the_field('blog_section_button'); ?></button>
              <?php endif; ?>
              	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read More...</a>
            </div>

          </div>


        </div>

      </div>
    </div>
    <!-- /article -->

  <?php endwhile; ?>
  <!-- end of the loop -->

  <!-- pagination here -->

  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>



<?php get_footer(); ?>
