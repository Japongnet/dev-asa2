<?php
/**
* Template Name: Basket Page
*
*/


get_header();



?>

<?php
$i = 0;
// check if the repeater field has rows of data
if( have_rows('page_sections_repeater') ):

  // loop through the rows of data
  while ( have_rows('page_sections_repeater') ) : the_row(); ?>

  <?php $i++; ?>

  <?php if( $i == 2  && !is_front_page()) {
    include 'header_menu.php'; ?>
    <div class="waypoint section background_highlight background_gradient bg_cover orient_left">
      <div class="section_content section_content_cart">
        <div class="cart_page cart_interior">
        </div>
        <div class="cart_contact">
          <?php echo do_shortcode( '[contact-form-7 id="530" title="Catering Order Form"] ' ); ?>
        </div>
      </div>

    </div>
<?php  } ?>



  <?php
  if ($time_of_day == get_sub_field('page_section_id') && get_sub_field('page_section_timed')){
    include 'content_repeater.php';
  };
  ?>

  <?php
  if (get_sub_field('page_section_timed')){

  } else {
    include 'content_repeater.php';
  }  ?>



<?php endwhile;

else :

  // no rows found

endif;

?>







<?php get_footer(); ?>
