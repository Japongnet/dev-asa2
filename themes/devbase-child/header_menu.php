<header class="header clear">
  <?php if(is_front_page()): ?>

  <?php endif; ?>
  <div class="menu_section background_primary sticky_bottom">
    <div class="menu_content ">


      <!-- nav -->
      <nav class="nav">



        <?php
        $i = 0;
        // check if the repeater field has rows of data
        if( have_rows('page_sections_repeater') ): ?>
        <div class="cart_interior">Cart Interior</div>
        <a href="#home"><div class="header_home_logo_icon bg_contain" style="background-image:url('<?php the_field('header_home_logo', 27);?>')"></div></a>
        <ul>
          <?php // loop through the rows of data
          while ( have_rows('page_sections_repeater') ) : the_row(); ?>
          <li><a class="test_scroll <?php the_sub_field('page_category_id'); ?>" href="#<?php the_sub_field('page_category_id'); ?>"><?php the_sub_field('page_category_id'); ?></a></li>
        <?php endwhile; ?>

        <?php
        else :

          // no rows found

        endif;

        ?>

      </ul>

    </nav>




    <!-- /nav -->

  </div>
</div>
<div class="menu_section_mobile background_primary sticky_bottom">
  <div class="menu_content ">


    <!-- nav -->
    <nav class="nav">


      <?php
      $i = 0;
      // check if the repeater field has rows of data
      if( have_rows('page_sections_repeater') ): ?>
      <div class="cart_interior">Cart Interior</div>
      <ul>
        <?php // loop through the rows of data
        while ( have_rows('page_sections_repeater') ) : the_row(); ?>
        <li><a class="test_scroll <?php the_sub_field('page_category_id'); ?>" href="#<?php the_sub_field('page_category_id'); ?>"><?php the_sub_field('page_category_id'); ?></a></li>
      <?php endwhile; ?>
    </ul>
    <?php
    else :

      // no rows found

    endif;

    ?>



  </nav>

  <a href="<?php echo site_url(); ?>"><div class="header_home_logo_icon bg_contain" style="background-image:url('<?php the_field('header_home_logo', 27);?>')"></div></a>
  <a href="#" class="menu_toggle">Menu Toggle</a>

  <!-- /nav -->

</div>
</div>

</header>





<span class="header_toggle">

</span>
